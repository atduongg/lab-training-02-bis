import logo from './logo.svg';
import './App.css';
import React from "react";
function App() {

  const [data, setData] = React.useState(null);

  React.useEffect(() => {
    fetch("http://localhost:8080/api")
      .then((res) => res.json())
      .then((data) => setData(data.message));
  }, []);
  return (
    <div className="App">
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <p>{!data ? "Au revoir Business Training et merci pour le café" : data}</p>
    </header>
  </div>
  );
}

export default App;
